const path = require('path');
const webpackNodeExternals = require('webpack-node-externals');
require('dotenv').config();

module.exports = {
  entry: {
    server: './src/bin/server.js',
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  target: 'node',
  node: {
    __dirname: false,
    __filename: false,
  },
  externals: [webpackNodeExternals()],
  module: {
    rules: [
      {
        // Transpiler from ES6 to ES5
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
    ],
  },
  mode: process.env.NODE_ENV,
};
